#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>
#include "controllers/CInputCtrl.h"
#include "controllers/CGAEngine.h"
using namespace std;

int main(int argc, char* argv[])
{
    if (argc >= 3)
    {
        srand(time(0));
        CInputCtrl input(argc, argv);
        CGAEngine::getInstance().run();

        return 0;
    }
    else
    {
        cerr << "Usage: ./voyageur-commerce <Nombre de generations> <Nombre d'individus par population> [Fichier d'entree]" << endl;
        return 1;
    }
}
