#ifndef ____CDISPLAYCTRL_H___
#define ____CDISPLAYCTRL_H___

#include <iostream>
#include <string>
#include <vector>
#include "CInputCtrl.h"
#include "../models/CClientTCP.h"
#include "../models/CColor.h"
#include "../models/CCityModel.h"
using namespace std;

class CDisplayCtrl
{
    private:
        CDisplayCtrl();
        ~CDisplayCtrl();
        static CDisplayCtrl sInstance;
        CClientTCP* mClient;

    public:
        static CDisplayCtrl& getInstance();
        void clean();
        void displayCity(CCityModel);
        void displayLink(CCityModel, CCityModel);
        void displayCitys();
};

#endif

