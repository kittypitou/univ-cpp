#ifndef ____CINPUTCMDCTRL_H____
#define ____CINPUTCMDCTRL_H____

#include <iostream>
#include <string>
#include <vector>
#include "../models/CCityModel.h"
using namespace std;

class CInputCmdCtrl
{
    private:
        vector<CCityModel> tableCoords;

    public:
        CInputCmdCtrl();
        vector<CCityModel> get();
        int getSize();
};

#endif

