#ifndef ____CPOPULATIONFACTORY_H____
#define ____CPOPULATIONFACTORY_H____

#include <iostream>
#include <string>
#include <vector>
#include "CInputCtrl.h"
#include "../models/CPopulationModel.h"
#include "../models/CRandomGeneratorModel.h"
#include "../models/CChromosomModel.h"
using namespace std;

class CPopulationFactory
{
    private:
        static const int sMAXI = 2;
        static int sNB_CHROM_BY_POP;
        static CPopulationFactory sInstance;
        vector<CPopulationModel> mPops;
        CPopulationFactory();
        ~CPopulationFactory();

    public:
        static CPopulationFactory& getInstance(); // Obtenir l'instance unique de la classe
        void create(); // Créer une nouvelle population si le nombre de populations existantes < sMAXI
        void createRandom(); // Créer une nouvelle population aléatoire
        void remove(int); // Supprime une population par son indice
        void setPop(int, CPopulationModel);
        void addChromToPop(int, CChromosomModel);
        CPopulationModel operator[](int); // Accéder à la population par son indice
        static int getMaxi();
        static void setMaxPops(int);
};

#endif
