#include "CGAEngine.h"

CGAEngine CGAEngine::sInstance = CGAEngine();
int CGAEngine::sCONVERGENCE = 100;

CGAEngine::CGAEngine()
{

}

void CGAEngine::run()
{
    // Affichage des villes
    CDisplayCtrl::getInstance().clean();
    CDisplayCtrl::getInstance().displayCitys();

    // Génération aléatoire de la première population
    CPopulationFactory::getInstance().createRandom();

    int cpt, i, j, rand1, rand2, rand3, rand4, rand_tmp, percent;
    vector<CChromosomModel> childrens, tmp_pop, elit;
    
    // Id du meilleur individu à afficher
    int best_path;
    
    for (cpt = 0; cpt < sCONVERGENCE; cpt++)
    {
        cout << "[" << (cpt + 1) << "/" << sCONVERGENCE << "] " << endl;

        CPopulationFactory::getInstance().create();

        // Création d'une collection d'individus temporaire, nécessaire pour le croisement
        tmp_pop = CPopulationFactory::getInstance()[0].getPop();
        
        // Elitisme
        elit = CGAOperator::elitism(tmp_pop);
        for (i = 0; i < elit.size(); i++)
        {
            CPopulationFactory::getInstance().addChromToPop(1, elit[i]);
        }
        // Les élites ont maintenant été supprimées de tmp_pop (car passé par référence), on peut croiser le reste

        // Pour tous les éléments restants (non élites), par paires de deux
        while (tmp_pop.size() >= 2)
        {
            // Croisement
            do
            {
                rand1 = CRandomGeneratorModel::random(tmp_pop.size());
                rand2 = CRandomGeneratorModel::random(tmp_pop.size());
            } while (rand1 == rand2);
            
            childrens = CGAOperator::crossover(tmp_pop[rand1], tmp_pop[rand2]);

            // Mutation (une chance sur dix d'être muté)
            rand3 = CRandomGeneratorModel::random(10);
            rand4 = CRandomGeneratorModel::random(10);
           
            if (rand3 == 5) // Nombre choisi arbitrairement parmi les 10 nombres possibles (0-9)
            {
                childrens[0] = CGAOperator::mutation(childrens[0]);
            }

            if (rand4 == 5)
            {
                childrens[1] = CGAOperator::mutation(childrens[1]);
            }

            // Ajout des enfants générés à la génération suivante
            CPopulationFactory::getInstance().addChromToPop(1, childrens[0]);
            CPopulationFactory::getInstance().addChromToPop(1, childrens[1]);
            
            /*
             * On supprime les deux parents en s'assurant de supprimer celui qui a l'indice le plus grand en premier
             * Cela permet à erase() de fonctionner correctement: supprimer le plus petit en premier modifierais
             * L'indice du deuxième, ce qui nous obligerait à "bidouiller"
             */

            if (rand2 > rand1)
            {
                rand_tmp = rand1;
                rand1 = rand2;
                rand2 = rand_tmp;
            }

            tmp_pop.erase(tmp_pop.begin() + rand1);
            tmp_pop.erase(tmp_pop.begin() + rand2);
        }
        
        // La nouvelle population devient l'ancienne, on peut recommencer le processus
        CPopulationFactory::getInstance().setPop(0, CPopulationFactory::getInstance()[1]);
        CPopulationFactory::getInstance().remove(1);
        
        /*cout << endl;
        for (i = 0; i < CPopulationFactory::getInstance()[0].getPop().size(); i++)
        {
            cout << CPopulationFactory::getInstance()[0][i].getFitness() << endl;
        }
        cout << endl;*/        
    }

    // La population "précédente" devient la population finale. Algorithme terminé.
    CPopulationFactory::getInstance().create();
    CPopulationFactory::getInstance().setPop(1, CPopulationFactory::getInstance()[0]);

    // Affichage de la meilleure solution
    best_path = 0;
    for (i = 1; i < CPopulationFactory::getInstance()[1].getPop().size(); i++)
    {
        if (CPopulationFactory::getInstance()[1][i].getFitness() < CPopulationFactory::getInstance()[1][best_path].getFitness())
        {
            best_path = i;
        }
    }
    
    for (i = 1; i < CPopulationFactory::getInstance()[1][best_path].size(); i++)
    {
        CDisplayCtrl::getInstance().displayLink(CPopulationFactory::getInstance()[1][best_path][i-1], CPopulationFactory::getInstance()[1][best_path][i]);
    }
    CDisplayCtrl::getInstance().displayLink(CPopulationFactory::getInstance()[1][best_path][i-1], CPopulationFactory::getInstance()[1][best_path][0]);
}

CGAEngine& CGAEngine::getInstance()
{
    return sInstance;
}

CPopulationModel CGAEngine::getInstantCapture()
{
    return CPopulationFactory::getInstance()[0];
}

CPopulationModel CGAEngine::getFinalPopulation()
{
    return CPopulationFactory::getInstance()[1];
}

void CGAEngine::setMaxLoops(int nb)
{
    sCONVERGENCE = nb;
}
