#include "CPopulationFactory.h"

CPopulationFactory CPopulationFactory::sInstance = CPopulationFactory();
int CPopulationFactory::sNB_CHROM_BY_POP = 100;

CPopulationFactory::CPopulationFactory()
{

}

CPopulationFactory::~CPopulationFactory()
{
    
}

CPopulationFactory& CPopulationFactory::getInstance()
{
    return sInstance;
}

void CPopulationFactory::create()
{
    if (mPops.size() < sMAXI)
    {
        CPopulationModel tmp_pop;
        mPops.push_back(tmp_pop); // Ajoute un élément à mPops
    }
    else
    {
        cerr << "ERREUR : nombre maximum de populations atteint" << endl;
    }
}

void CPopulationFactory::createRandom()
{
    if (mPops.size() == 0)
    {   
        CCityModel null_city = CCityModel(-1, 0, 0);
        getInstance().create();
        int i, j, random;
        vector<CCityModel> tmp_citys;
        vector<CCityModel> citys;
        for (i = 0; i < sNB_CHROM_BY_POP; i++)
        {
            tmp_citys.resize(0, null_city);
            citys = CInputCtrl::getCitys();
            for (j = 0; j < CInputCtrl::getNbCitys(); j++)
            {
                random = CRandomGeneratorModel::random(citys.size());
                tmp_citys.push_back(citys[random]);
                citys.erase(citys.begin() + random);
            }
            mPops[0].addChrom(CChromosomModel(tmp_citys));
        }
    }
}

void CPopulationFactory::remove(int i)
{
    if (mPops.size() > i && &mPops[i] != NULL)
    {
        mPops.erase(mPops.begin() + i); // erase() efface une case du vecteur, begin() permet d'obtenir l'indice du premier élément
    }
}

void CPopulationFactory::setPop(int i, CPopulationModel pop)
{
    if (mPops.size() > i)
    {
        mPops[i] = pop;
    }
}

CPopulationModel CPopulationFactory::operator[](int i)
{
    return mPops[i];
}

int CPopulationFactory::getMaxi()
{
    return sMAXI;
}

void CPopulationFactory::addChromToPop(int i, CChromosomModel chrom)
{
    mPops[i].addChrom(chrom);
}

void CPopulationFactory::setMaxPops(int nb)
{
    sNB_CHROM_BY_POP = nb;
}
