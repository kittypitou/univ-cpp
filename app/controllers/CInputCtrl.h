#ifndef ____CINPUTCTRL_H____
#define ____CINPUTCTRL_H____

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "CInputFileCtrl.h"
#include "CInputCmdCtrl.h"
#include "CDisplayCtrl.h"
#include "CPopulationFactory.h"
#include "CGAEngine.h"
#include "../models/CDistanceModel.h"
#include "../models/CCityModel.h"
using namespace std;

class CInputCtrl
{
    private:
        static vector<CCityModel> sCitys;
        static int sNbCitys;

    public:
        CInputCtrl(int, char**);
        static int getNbCitys();
        static CCityModel getCity(int);
        static vector<CCityModel> getCitys();
};

#endif
