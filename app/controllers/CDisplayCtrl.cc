#include "CDisplayCtrl.h"

CDisplayCtrl CDisplayCtrl::sInstance = CDisplayCtrl();

CDisplayCtrl::CDisplayCtrl()
{
    mClient = new CClientTCP("127.0.0.1", 1500);
}

CDisplayCtrl::~CDisplayCtrl()
{
    mClient->disconnect();
}

CDisplayCtrl& CDisplayCtrl::getInstance()
{
    return sInstance;
}

void CDisplayCtrl::clean()
{
    mClient->clear();
    /*
     * Bug de l'afficheur JAVA: affichage d'un cercle de rayon 0 en 0,0 car le
     * premier objet après un clear() ne s'affiche pas
     */
    mClient->drawCircle(0, 0, 0, CColor::sWhite);
}

void CDisplayCtrl::displayCity(CCityModel city)
{
    mClient->drawCircle(city.getX(), city.getY(), 10, CColor::sRed, true);
}

void CDisplayCtrl::displayLink(CCityModel v1, CCityModel v2)
{
    mClient->drawSegment(v1.getX(), v1.getY(), v2.getX(), v2.getY(), CColor::sWhite);
}

void CDisplayCtrl::displayCitys()
{
    int i;
    for (i = 0; i < CInputCtrl::getNbCitys(); i++)
    {
        getInstance().displayCity(CInputCtrl::getCity(i));
    }
}
