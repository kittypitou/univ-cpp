#include "CInputCmdCtrl.h"

CInputCmdCtrl::CInputCmdCtrl()
{
    bool continuer = true;
    int x_tmp;
    int y_tmp;
    int i = 0;
    while (continuer)
    {
        x_tmp = -1;
        y_tmp = -1;

        cout << "Entrer coordonnées x,y : " << endl << "x: ";
        cin >> x_tmp;
        if (x_tmp == -1)
        {
            continuer = false;
            continue;
        }

        cout << "y: ";
        cin >> y_tmp;
        if (y_tmp == -1)
        {
            continuer = false;
            continue;
        }
        
        if (x_tmp != -1 && y_tmp != -1)
        {
            tableCoords.push_back(CCityModel(i, x_tmp, y_tmp));
        }
        i++;
    } 
}

vector<CCityModel> CInputCmdCtrl::get()
{
    return tableCoords;
}

int CInputCmdCtrl::getSize()
{
    return tableCoords.size();
}