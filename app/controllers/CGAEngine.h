#ifndef ____CGAENGINE_H____
#define ____CGAENGINE_H____

#include <iostream>
#include <string>
#include <vector>
#include "CGAOperator.h"
#include "CPopulationFactory.h"
#include "CDisplayCtrl.h"
#include "../models/CChromosomModel.h"
#include "../models/CRandomGeneratorModel.h"
#include "../models/CPopulationModel.h"
using namespace std;

// Singleton
class CGAEngine
{
    private:
        CPopulationModel* mCurrentPop;
        CPopulationModel* mNextPop;
        static int sCONVERGENCE;
        static CGAEngine sInstance;
        CGAEngine();

    public:
        static CGAEngine& getInstance();
        void run();
        CPopulationModel getInstantCapture();
        CPopulationModel getFinalPopulation();
        static void setMaxLoops(int);
};

#endif

