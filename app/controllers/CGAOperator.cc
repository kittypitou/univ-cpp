#include "CGAOperator.h"

vector<CChromosomModel> CGAOperator::elitism(vector<CChromosomModel>& chrom)
{
    vector<CChromosomModel> tmp_chrom;
    int i;
    int j;
    float percent = chrom.size() / 10;
    float mini;
    int k;
    for(i=0; i < percent ; i++)
    {
        tmp_chrom.push_back(chrom[0]);
        mini = chrom[0].getFitness();
        k = 0;
        for(j=1; j < chrom.size(); j++)
        {
            if (chrom[j].getFitness() < mini)
            {
                mini = chrom[j].getFitness();
                k = j;
            }
        }
        tmp_chrom[i] = chrom[k];
        chrom.erase(chrom.begin()+k);
    }
    return tmp_chrom;
}

CChromosomModel CGAOperator::mutation(CChromosomModel chrom)
{
    int randomIndice1, randomIndice2;
    CCityModel tmp(-1, 0, 0);
    do
    {
        randomIndice1 = CRandomGeneratorModel::random(chrom.size());
        randomIndice2 = CRandomGeneratorModel::random(chrom.size());
    } while (randomIndice1 == randomIndice2);

    tmp = chrom[randomIndice1];
    chrom.setGene(randomIndice1, chrom[randomIndice2]);
    chrom.setGene(randomIndice2, tmp);
    chrom.setFitness();
    return chrom;
}

vector<CChromosomModel> CGAOperator::crossover(CChromosomModel chrom1, CChromosomModel chrom2)
{
    float percent = 40;
    int i, j;
    vector<CChromosomModel> vect_chrom(2, chrom1);
    int maxi = chrom1.size()*(percent/100);
    int already, current;

    for(i=0; i < maxi; i++)
    {
        current = i;
        do
        {
            already = -1;
            for (j=maxi; j < chrom1.size() && already == -1; j++)
            {
                if (vect_chrom[1][j].getId() == chrom2[current].getId())
                {
                    already = j;
                    current = j;
                }
            }
        } while (already != -1);
        
        vect_chrom[1].setGene(i, chrom2[current]);
        //vect_chrom[1][i] = chrom2[i];
    }

    for(i=maxi; i < chrom1.size(); i++)
    {
        current = i;
        do
        {
            already = -1;
            for (j=0; j < maxi && already == -1; j++)
            {
                if (vect_chrom[0][j].getId() == chrom2[current].getId())
                {
                    already = j;
                    current = j;
                }
                }
        } while (already != -1);
        
        vect_chrom[0].setGene(i, chrom2[current]);
        //vect_chrom[0][i] = chrom2[i];
    }
    
    vect_chrom[0].setFitness();
    vect_chrom[1].setFitness();

    return vect_chrom;
}
