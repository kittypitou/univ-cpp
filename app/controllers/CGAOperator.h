#ifndef ____CGAOPERATOR_H____
#define ____CGAOPERATOR_H____

#include <iostream>
#include <string>
#include <vector>
#include "../models/CRandomGeneratorModel.h"
#include "../models/CChromosomModel.h"
#include "../models/CCityModel.h"
using namespace std;

class CGAOperator
{
    public:
        static vector<CChromosomModel> elitism(vector<CChromosomModel>&);
        static CChromosomModel mutation(CChromosomModel);
        static vector<CChromosomModel> crossover(CChromosomModel, CChromosomModel);
};

#endif