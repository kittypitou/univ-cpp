#ifndef ____CINPUTFILECTRL_H____
#define ____CINPUTFILECTRL_H____

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include "../models/CCityModel.h"

using namespace std;

class CInputFileCtrl
{
    private:
        ifstream* mFile;
        vector<CCityModel> mCitys;     

    public:
        CInputFileCtrl(char*);
        vector<CCityModel> parse();
        int getSize();
};

#endif

