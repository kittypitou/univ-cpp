#include "CInputCtrl.h"

vector<CCityModel> CInputCtrl::sCitys = vector<CCityModel>();
int CInputCtrl::sNbCitys = 0;

CInputCtrl::CInputCtrl(int argc, char* argv[])
{
    CCityModel null_city(-1, 0, 0);
    if (argc == 4)
    {
        CInputFileCtrl filectrl(argv[3]);
        sCitys.resize(sNbCitys, null_city);
        sCitys = filectrl.parse();
        sNbCitys = filectrl.getSize(); // getSize(): permet de connaitre le nombre de villes
    }
    else
    {
        CInputCmdCtrl inputcmd;
        sNbCitys = inputcmd.getSize(); // getSize(): permet de connaitre le nombre de villes
        sCitys.resize(sNbCitys, null_city); 
        sCitys = inputcmd.get();
    }
    
    CGAEngine::setMaxLoops(atoi(argv[1]));
    CPopulationFactory::setMaxPops(atoi(argv[2]));
    
    CDistanceModel::getInstance().init(sCitys);
}

int CInputCtrl::getNbCitys()
{
    return sNbCitys;
}

CCityModel CInputCtrl::getCity(int i)
{
    return sCitys[i];
}

vector<CCityModel> CInputCtrl::getCitys()
{
    return sCitys;
}
