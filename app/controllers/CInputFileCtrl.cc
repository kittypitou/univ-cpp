#include "CInputFileCtrl.h"

using namespace std;

CInputFileCtrl::CInputFileCtrl(char* pFile)
{
    mFile = new ifstream(pFile);
    if (!(*mFile))
    {
        cerr << "ERREUR : Impossible d'ouvrir le fichier" << endl;
    }
}

vector<CCityModel> CInputFileCtrl::parse()
{
    string line;
    int i = 0, x, y;
    while (getline((*mFile), line))
    {
        istringstream iss(line);
        if (!(iss >> x >> y))
        {
            break;
        }
        mCitys.push_back(CCityModel(i, x, y));
        i++;
    }

    return mCitys;
}

int CInputFileCtrl::getSize()
{
    return mCitys.size();
}
