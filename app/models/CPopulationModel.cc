#include "CPopulationModel.h"

CPopulationModel::CPopulationModel(vector<CChromosomModel> vectChrom)
{
    int i;
    if (vectChrom.size() > 0)
    {
        for (i = 0; i < vectChrom.size(); i++)
        {
            mPop.push_back(vectChrom[i]);
        }
    }
}

CChromosomModel CPopulationModel::operator[](int i)
{
    return mPop[i];
}

void CPopulationModel::addChrom(CChromosomModel chrom)
{
    mPop.push_back(chrom);
}

vector<CChromosomModel> CPopulationModel::getPop()
{
    return mPop;
}