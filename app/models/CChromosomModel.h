#ifndef ____CCHROMOSOMMODEL_H____
#define ____CCHROMOSOMMODEL_H____

#include <iostream>
#include <string>
#include <vector>
#include "CDistanceModel.h"
#include "../models/CCityModel.h"
using namespace std;

class CChromosomModel
{
    private:
        vector<CCityModel> mGenes;
        float mFitness;

    public:
        CChromosomModel(vector<CCityModel> = vector<CCityModel>());
        float getFitness();
        void setFitness();
        CCityModel operator[](int);
        int size();
        void setGene(int, CCityModel);
};

#endif

