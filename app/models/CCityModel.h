#ifndef ____CCITYMODEL____
#define ____CCITYMODEL____

#include <iostream>
#include <string>
#include <vector>
using namespace std;

class CCityModel
{
    private:
        int mId;
        int mX;
        int mY;

    public:
        int getX();
        int getY();
        int getId();
        CCityModel(int, int, int);
};

#endif

