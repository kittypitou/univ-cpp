#ifndef ____CRANDOMGENERATORMODEL_H____
#define ____CRANDOMGENERATORMODEL_H____

#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>
using namespace std;

class CRandomGeneratorModel
{
    public:
        static int random(int, int randmin = 0);
};

#endif

