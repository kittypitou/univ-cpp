#include "CChromosomModel.h"

CChromosomModel::CChromosomModel(vector<CCityModel> genes)
{
    mFitness = 0;
    int i;
    for (i = 0; i < genes.size(); i++)
    {
        mGenes.push_back(genes[i]);
    }
    setFitness();
}

float CChromosomModel::getFitness()
{
    return mFitness;
}

void CChromosomModel::setFitness()
{
    mFitness = 0;
    int i;
    for (i = 0; i < mGenes.size(); i++)
    {
        if (i > 0)
        {
            mFitness += CDistanceModel::getInstance().getDistance(mGenes[i-1], mGenes[i]);
        }
    }  
    mFitness += CDistanceModel::getInstance().getDistance(mGenes[i-1], mGenes[0]);
}

CCityModel CChromosomModel::operator[](int i)
{
    return mGenes[i];
}

int CChromosomModel::size()
{
    return mGenes.size();
}

void CChromosomModel::setGene(int i, CCityModel gene)
{
    mGenes[i] = gene;
}
