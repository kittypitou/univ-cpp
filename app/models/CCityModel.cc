#include "CCityModel.h"

CCityModel::CCityModel(int id, int x, int y)
{
    mX = x;
    mY = y;
    mId = id;
}

int CCityModel::getX()
{
    return mX;
}

int CCityModel::getY()
{
    return mY;
}

int CCityModel::getId()
{
    return mId;
}