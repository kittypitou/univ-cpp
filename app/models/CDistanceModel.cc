#include "CDistanceModel.h"

CDistanceModel CDistanceModel::sInstance = CDistanceModel();

CDistanceModel::CDistanceModel()
{

}

CDistanceModel::~CDistanceModel()
{

}

void CDistanceModel::init(vector<CCityModel> citysList)
{
    int i, j, distX, distY;
    for (i = 0; i < citysList.size(); i++ )
    {
        mMap.push_back(vector<float>(citysList.size()));
        for (j = 0; j < citysList.size(); j++ )
        {
            distX = abs(citysList[i].getX() - citysList[j].getX());
            distY = abs(citysList[i].getY() - citysList[j].getY());
            mMap[citysList[i].getId()][citysList[j].getId()] = sqrt((distX * distX) + (distY * distY));
        }
    }
}
CDistanceModel& CDistanceModel::getInstance(vector<CCityModel> citysList)
{
    return sInstance;
}

float CDistanceModel::getDistance(CCityModel v1, CCityModel v2)
{
    return mMap[v1.getId()][v2.getId()];
}