#ifndef ____CDISTANCEMODEL____
#define ____CDISTANCEMODEL____

#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include "CCityModel.h"
using namespace std;

// Singleton
class CDistanceModel
{
    private:
        static CDistanceModel sInstance;
        CDistanceModel();
        ~CDistanceModel();
        vector<vector<float> > mMap;

    public:
        static CDistanceModel& getInstance(vector<CCityModel> citysList = vector<CCityModel>());
        void init(vector<CCityModel>);
        float getDistance(CCityModel, CCityModel);
};

#endif

