#ifndef ____CPOPULATIONMODEL____
#define ____CPOPULATIONMODEL____

#include <iostream>
#include <string>
#include <vector>
#include "CChromosomModel.h"
using namespace std;

class CPopulationModel
{
    private:
        vector<CChromosomModel> mPop;

    public:
        CPopulationModel(vector<CChromosomModel> vectChrom = vector<CChromosomModel>());
        CChromosomModel operator[](int);
        void addChrom(CChromosomModel);
        vector<CChromosomModel> getPop();
    };

#endif

