#include <iostream>
#include <errno.h>

#include "CClientTCP.h"

using namespace std;

int main()
{

    CClientTCP client( "127.0.0.1", 1500 );

    client.drawPoint( 5, 10, CColor::sGreen );                       // affiche un point de couleur vert
    client.drawSegment( 15, 10, 25, 30, CColor::sRed );              // affiche un segment de couleur rouge
    client.drawCircle( 20, 150, 10, CColor::sCyan );                 // affiche un cercle de couleur cyan et de rayon 10 
    client.drawCircle( 25, 50, 5, CColor::sMagenta, true );          // affiche un cercle plein de couleur magenta et de rayon 5
    client.drawRectangle( 50, 100, 20, 30, CColor::sYellow, true );  // affiche un rectangle plein de couleur jaune
    client.drawRectangle( 200, 100, 250, 300, CColor::sGreen );      // affiche un rectangle de couleur vert
    
    client.disconnect();
    

    return 0;
}
